import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListPractice {

    LinkedList<String> l1;

    public void createLinkedlist()
    {

        l1= new LinkedList<>();
        l1.add("Ayush");
        l1.add("Arjita");
        l1.add("Arpita");
        l1.add("Adi");
    }

    public void printlist()
    {
        Iterator it= l1.iterator();

        while(it.hasNext())
        {
            System.out.println(it.next());
        }

    }

    public void checkifpresent()
    {

        if(l1.contains("Adi"));
        {
            System.out.println("the name is present ");
        }

    }

    public static void main(String[] args) {

        LinkedListPractice lp= new LinkedListPractice();

        lp.createLinkedlist();
        lp.printlist();
        lp.checkifpresent();



    }


}
