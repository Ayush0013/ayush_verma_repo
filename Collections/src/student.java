public class student {

   private String name ;
  private String College;
  static int interest=6;

    public student(String name, String college) {
        this.name = name;
        College = college;
    }

    public int getInterest() {
        return interest;
    }


    public String getName() {
        return name;
    }

    public String getCollege() {
        return College;
    }

    @Override
    public String toString() {
        return "student{" +
                "name='" + name + '\'' +
                ", College='" + College + '\'' +
                '}';
    }
}
