package Basic;

import java.lang.Math;

public class basicmath {

    public int add(int a, int b )
    {
        return a+b;
    }

    public  int substract (int a, int b)
    {
        return a-b;
    }

    public int reverse(int a)
    {
        int power=0;

        int rev=0;
        while(a!=0)
        {

            int rem=a%10;
            rev=rev*10+rem;

            a=a/10;

        }
        return rev;


    }

    public int fact(int num)
    {
        int factorial=1;
        for(int i=1;i<num;i++)
        {
            factorial=factorial+factorial*i;


        }
        return  factorial;

    }
}
