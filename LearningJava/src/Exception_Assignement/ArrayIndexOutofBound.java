package Exception_Assignement;

public class ArrayIndexOutofBound {

    public static void main(String[] args) {

        int a[]= {1,2,3,4,5};

        for(int i=0 ; i<=a.length ; i++)
        {
            // length of the array is 5 hence a[0] to a[4] when
            // the Loop try to access a[5] we get index out of bound exception
            try {
                System.out.println(a[i]);
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                System.out.println("you are accessing the location which is not present in Array");
            }
            finally {
                continue;
            }
        }






    }

}
