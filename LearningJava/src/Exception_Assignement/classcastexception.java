package Exception_Assignement;

public class classcastexception {

    public void base()
    {
        System.out.println("this is base class");
    }
}


class c extends classcastexception
{

    public static void main(String[] args) {

        classcastexception obj = new classcastexception();
        try {
            c Bobj = (c)obj;
        }
        catch (ClassCastException e)
        {
            System.out.println("ClasscasteException class is not the sub class of C hence this Exception");
        }

        finally {
            System.out.println("this is finally block");
        }
    }
}
