package Exercise.ques2;

abstract class Book{

       String title;

    abstract void setTitle(String s);

    String getTitle(){

        return title;

    }

}

class myBook extends Book
{


    @Override
    void setTitle(String s) {

        super.title=s;

    }

    public static void main(String[] args) {

        Book bb = new myBook();

        bb.setTitle("Gita");

        System.out.println("the title of the book is " + bb.getTitle());
    }
}