package Exercise.ques4;

import org.w3c.dom.ls.LSOutput;

class Cycle {

    public void msg()
    {
        System.out.println("My ancestor is a cycle who is a vehicle with pedals.");
    }


}

class Bike extends Cycle
{

    public void msg()
    {
        System.out.println("Hello I am a motorcycle, I am a cycle with an engine.");
    }


     void callmsg()
    {
        super.msg();
    }

    public static void main(String[] args) {


       Bike bk= new Bike();
       bk.msg();
       bk.callmsg();




    }

}
