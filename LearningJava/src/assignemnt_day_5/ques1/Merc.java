package assignemnt_day_5.ques1;

public class Merc extends Car {

    public void display()
    {
        System.out.println("this is a merc display");
    }

    public static void main(String[] args) {

        System.out.println(" ---obj type is car and its refring to Merc class ---");
        Car cr = new Merc();
        cr.display();

        System.out.println("---obj type is car and its refring to Car class ---");
        Car cr2 = new Car();
        cr2.display();


        System.out.println("---obj type is Merc and refering to merc---");

        Merc mr= new Merc();
        mr.display();

    }
}
