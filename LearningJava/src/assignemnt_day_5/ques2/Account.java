package assignemnt_day_5.ques2;



import java.util.Scanner;

public class Account extends BaseAccount{

    private String Name ;
    private String AccountNO;
    private int Amount ;
    private int SavMinBal=1000;
    private int CurrMinBal=2000;
    private String Acctype;
    private Boolean depositflag=true;

    public String getAcctype() {
        return Acctype;
    }

    public String getName() {
        return Name;
    }

    public String getAccountNO() {
        return AccountNO;
    }

    public int getAmount() {
        return Amount;
    }

    public Account() {
    }

    public Account(String name, String accountNO) {
int openAmount=0;
        Scanner sc = new Scanner(System.in);

        System.out.print("please enter Account Type :--> ");
        String acctype = sc.next();

        Acctype = acctype;

        System.out.println();


        while(depositflag) {
            System.out.print("Please Enter the opening  :--> ");
             openAmount = sc.nextInt();


            if(getAcctype().equalsIgnoreCase("saving") && openAmount>SavMinBal)
            {
                depositflag=false;
            }
            else if(getAcctype().equalsIgnoreCase("Current") && openAmount > CurrMinBal)
            {
                depositflag=false;
            }
            else
            {
                System.out.println(" the opening amount is Less than Minimum Opening Balance");
            }

        }

            Name = name;
            AccountNO = accountNO;

            Amount = openAmount;



    }



    @Override
    public void deposit() {
        int Money=0;

            System.out.print("*** enter the Money to be deposited ****");
            Scanner sc = new Scanner(System.in);

          Money = sc.nextInt();




        System.out.println();


        Amount=Amount+Money;
        System.out.println( Money + "  Deposited in your Account ending with  xxxx" + getAccountNO().substring(4,getAccountNO().length())  );




    }

    @Override
    public void withdrawal(int Money) {

        Amount=Amount-Money;

    }

    @Override
    public String toString() {
        return "Account{" +
                "Name='" + Name + '\'' +
                ", AccountNO='" + AccountNO + '\'' +
                ", Amount=" + Amount +
                ", Acctype='" + Acctype + '\'' +
                '}';
    }
}
